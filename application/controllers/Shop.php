<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Shop extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$language = $this->session->userdata('language');
		$price = $this->session->userdata('price');
		$this->lang->load('content' , $language);
		$this->lang->load('price' , $price);
		$this->load->model('admin_model','admin');

	}

	public function index()
	{

		$data['price'] = $this->admin->view();
		foreach($data['price'] as $p);

		function CBR_XML_Daily_Ru() {
			static $rates;

			if ($rates === null) {
				$rates = json_decode(file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js'));
			}

			return $rates;
		}

		$data1 = CBR_XML_Daily_Ru();
		$ru = $data1->Valute->AMD->Value;
		$en = $data1->Valute->USD->Value;
		$rur = preg_replace('/\..*/','',$ru)*($p['price']/100);
		$data['price']= array(
			'amd' => $p['price'].'֏ ',
			'rur' => preg_replace('/\..*/','',$ru)*($p['price']/100).'₽ ',
			'usd' => preg_replace('/\..*/','',($rur)/preg_replace('/\..*/','',$en)).'$'

		);
		$language =$this->session->userdata('language');
		$price = $this->session->userdata('price');
		$this->lang->load('content', $language);
		$this->lang->load('price' , $price);
		$this->load->view('shop',$data );
	}
	public function setLanguage(){
		$language = $this->input->post('lan');
		$this->session->set_userdata('language' , $language);
	}
	public function setPrice(){
		$price = $this->input->post('price');
		$this->session->set_userdata('price' , $price);
	}


}
