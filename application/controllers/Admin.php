<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model','admin');
	}

	public function index()
	{
		$this->load->view('/admin/admin' );

	}

	public function update(){
		$data['price'] = $this->input->post('price');


		if ($this->admin->update($data)) {
			redirect(base_url('shop'));
		}
		else {
			redirect(base_url('shop'));
		}

	}
}
