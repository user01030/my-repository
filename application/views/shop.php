<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
	<link rel="stylesheet" href="/assets/style.css">


	<title>Hello, world!</title>

</head>
<body>

<div class="container">
	<h6 align="right"><a href="<?php base_url() ;?>/Admin" class="btn btn-outline-dark mt-5">Admin panel</a></h6>
	<div class="row">
		<div class="col-2">
		</div>
		<div class="col-8 mt-5" >
			<div class="card mb-3" style="max-width: 700px; ">
				<div class="row no-gutters">
					<div class="col-md-5" >
						<img src="/pics/001.jpg" class="card-img" alt="img" height="100%">
					</div>
					<div class="col-md-7">
						<div class="car-body ml-5">
							<br>
							<h5 class="card-title mr-2" align="right">
								<div class="btn-group ">
									<div class="btn-group">
										<a href="#" class="btn btn-outline-dark btn-sm " id="cur" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20"><?php echo $this->lang->line('priceShort')?></i></a>
										<div class="dropdown-menu"  aria-labelledby="cur">
											<?php if( $this->lang->line('priceShort') === '$'): ?>
												<a class="dropdown-item" id ="rur" onclick="priceChange('russian')" href="#">₽</a>
												<a class="dropdown-item" id ="amd" onclick="priceChange('armenian')" href="#">֏</a>
											<?php endif; ?>
											<?php  if($this->lang->line('priceShort') === '₽'): ?>
												<a class="dropdown-item" id="usd" onclick="priceChange('english')" href="#">$</a>
												<a class="dropdown-item" id ="amd" onclick="priceChange('armenian')" href="#">֏</a>
											<?php endif; ?>

											<?php if($this->lang->line('priceShort') === '֏'):?>
												<a class="dropdown-item" id="usd" onclick="priceChange('english')" href="#">$</a>
												<a class="dropdown-item" id ="rur" onclick="priceChange('russian')" href="#">₽</a>
											<?php endif; ?>
										</div>
									</div>
									<div class="btn-group">
										<a href="#" class="btn btn-outline-dark btn-sm  " id="lang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20"><?php echo $this->lang->line('short')?></a>
										<div class="dropdown-menu"  aria-labelledby="lang">
											<?php if( $this->lang->line('short') === 'EN'): ?>
												<a class="dropdown-item " id="ru" href="#" onclick="languagesChange('russian')">RU</a>
												<a class="dropdown-item" id="hy" href="#" onclick="languagesChange('armenian')">HY</a>
											<?php endif; ?>
											<?php  if($this->lang->line('short') === 'RU'): ?>
												<a class="dropdown-item" id="en" href="#" onclick="languagesChange('english')">EN</a>
												<a class="dropdown-item" id="hy" href="#" onclick="languagesChange('armenian')">HY</a>
											<?php endif; ?>

											<?php if($this->lang->line('short') === 'HY'):?>
												<a class="dropdown-item " id="ru" href="#" onclick="languagesChange('russian')">RU</a>
												<a class="dropdown-item" id="en" href="#" onclick="languagesChange('english')">EN</a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</h5>
							<br>
							<p class="card-text"><b><?php echo $this->lang->line('name') ?>: MOCASSIN</b></p>
							<p class="card-text"><b><?php echo $this->lang->line('material') ?>: <?php echo $this->lang->line('mat') ?></b></p>
							<p class="card-text"><b><?php echo $this->lang->line('origin') ?>: Portugal</b></p>
							<p class="card-text"><b><?php echo $this->lang->line('manufacturer') ?>: Pablosky</b></p>
							<br>
							<?php if( $this->lang->line('priceShort') === '$'): ?>
							<p class="card-text"><b><?php echo $this->lang->line('price') ?>  <span id="pr"><?php echo $price['usd'] ?></span></b></p>
							<?php endif; ?>
							<?php if( $this->lang->line('priceShort') === '₽'): ?>
								<p class="card-text"><b><?php echo $this->lang->line('price') ?>  <span id="pr"><?php echo $price['rur'] ?></span></b></p>
							<?php endif; ?>
							<?php if( $this->lang->line('priceShort') === '֏'): ?>
								<p class="card-text"><b><?php echo $this->lang->line('price') ?>  <span id="pr"><?php echo $price['amd'] ?></span></b></p>
							<?php endif; ?>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<ul>

</ul>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script type="text/javascript">
	function languagesChange(lan) {
		$.ajax({
			type:"POST",
			url:"<?php echo base_url(); ?>Shop/setLanguage",
			data:{'lan':lan},
			success:function (data) {
				location.reload();

			}
		});
	}
	function priceChange(price) {
		$.ajax({
			type:"POST",
			url:"<?php echo base_url(); ?>Shop/setPrice",
			data:{'price':price},
			success:function (data) {
				location.reload();

			}
		});
	}


</script>
</body>
</html>
