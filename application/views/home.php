<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="/assets/style.css">
	<title>Document</title>
	<style>

		body{
			background-image: url(/pics/002.jpg);
			background-repeat: no-repeat;
			background-clip: border-box;
			background-origin: border-box;
		}

	</style>
</head>

<body>

<div id="container">

	<div id="hay" >
		<a href="#" onclick="language('armenian'); price('armenian')">Հայերեն</a>
		<div class="hay"></div>
	</div>

	<div id="rus">
		<a href="#" onclick="language('russian'); price('russian')">Русский</a>
		<div class="rus"></div>
	</div>

	<div id="eng">
		<a href="#"  onclick="language('english') ; price('english')">English</a>
		<div class="eng"></div>
	</div>

</div>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
</body>
</html>
<script>
	function language(lan) {
		$.ajax({
			type:"POST",
			url:"<?php echo base_url(); ?>Shop/setLanguage",
			data:{'lan':lan},
			success:function (data) {
				window.location = "http://taskci/shop";
			}
		});
	}
	function price(price) {
		$.ajax({
			type:"POST",
			url:"<?php echo base_url(); ?>Shop/setPrice",
			data:{'price':price},
			success:function (data) {
				console.log('ok')

			}
		});
	}
</script>
