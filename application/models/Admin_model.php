<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Admin_model extends CI_Model
{
	private $_table = 'items';


	public function view(){
		$query = $this->db->get('items');
		return $query->result_array();
	}

	public function update($data)
	{
		$query = $this->db->update($this->_table, $data, array('ID' => 1));

		return $query;
	}
}
