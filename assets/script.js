//language functions

let lang = document.getElementById("lang");
let langMenu = document.getElementById("lang-menu");
let label = document.getElementById("label");

let hy = document.getElementById('hy');
let ru = document.getElementById("ru");
let en = document.getElementById("en");

lang.addEventListener("click",function(){
  if(langMenu.style.display=="none"){langMenu.style.display="block"}
  else{langMenu.style.display="none"}
})


hy.addEventListener("click",function(){
  label.innerHTML = "HY";
  label.style.marginLeft="35px";
  hy.style.display =  "none";
  en.style.display="block";
  ru.style.display="block";
  langMenu.style.height = "49px";
})

ru.addEventListener("click",function(){
  label.innerHTML= "RU";
  label.style.marginLeft="35px"
  ru.style.display =  "none";
  hy.style.display="block";
  en.style.display="block";
  langMenu.style.height = "49px";
})

en.addEventListener("click",function(){
  label.innerHTML="EN";
  label.style.marginLeft="35px";
  en.style.display =  "none";
  hy.style.display="block";
  ru.style.display="block";
  langMenu.style.height = "49px";
})

//Money functions

let moneyBtn = document.getElementById("money-btn");
let moneyMenu = document.getElementById("money-menu")

let dram = document.getElementById("dram");
let rubli = document.getElementById("rubli");
let dollar = document.getElementById("dollar");

moneyBtn.addEventListener("click",function(){
  if(moneyMenu.style.display=="none"){moneyMenu.style.display="block"}
  else{moneyMenu.style.display="none"}
})


dram.addEventListener("click",function(){
  moneyBtn.innerHTML = "֏";
  dram.style.display =  "none";
  rubli.style.display="block";
  dollar.style.display="block";
})

rubli.addEventListener("click",function(){
  moneyBtn.innerHTML = "₽";
  dram.style.display = "block";
  rubli.style.display="none";
  dollar.style.display="block";
})

dollar.addEventListener("click",function(){
  moneyBtn.innerHTML = "$";
  dram.style.display = "block";
  rubli.style.display="block";
  dollar.style.display="none";
})

